package org.gabmus.veterinary;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Veterinary vet = new Veterinary();
        List<Thread> threadPool = new ArrayList<>();
        Animal[] animals = {
                new Cat(vet),
                new Dog(vet),
                new Dog(vet),
                new Cat(vet),
                new Cat(vet),
                new Cat(vet),
                new Cat(vet),
                new Dog(vet),
                new Dog(vet),
                new Dog(vet),
                new Cat(vet),
                new Cat(vet),
                new Dog(vet),
                new Dog(vet),
                new Cat(vet)
        };
        for (Animal animal: animals) {
            Thread t = new Thread(animal);
            threadPool.add(t);
            t.start();
        }
        for (Thread t: threadPool) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
