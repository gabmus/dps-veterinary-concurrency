package org.gabmus.veterinary;

import java.util.ArrayList;
import java.util.List;

public class Veterinary {
    private List<Animal> animals = new ArrayList<>();

    private int getNumAnimals(Class<?> type) {
        int num = 0;
        for (Animal animal: animals) {
            if (animal.getClass() == type) num++;
        }
        return num;
    }

    public synchronized int getCats() {
        return getNumAnimals(Cat.class);
    }

    public synchronized int getDogs() {
        return getNumAnimals(Dog.class);
    }

    public synchronized void enter(Animal animal) {
        animals.add(animal);
        System.out.println("===========");
        for (Animal a: animals) System.out.println(a);
        System.out.println("===========");
    }

    public synchronized void exit(Animal animalToExit) {
        for (int i=0; i<animals.size(); i++) {
            if (animals.get(i) == animalToExit) {
                animals.remove(i);
                break;
            }
        }
        notifyAll();
    }
}
