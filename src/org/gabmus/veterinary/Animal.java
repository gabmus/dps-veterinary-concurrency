package org.gabmus.veterinary;

import java.util.Random;

public abstract class Animal implements Runnable {

    private static final int MAX_WAIT = 5000;
    private static final int MIN_WAIT = 500;
    private static final Random rand = new Random();

    protected Veterinary vet;

    abstract boolean canEnter();

    public Animal(Veterinary vet) {
        this.vet = vet;
    }

    private void doVisit() {
        try {
            Thread.sleep(rand.nextInt(MAX_WAIT-MIN_WAIT) + MIN_WAIT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            synchronized (vet) {
                while (!canEnter()) {
                    vet.wait();
                }
            }
            vet.enter(this);
            doVisit();
            vet.exit(this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return this.getClass().getSimpleName();
    }
}
