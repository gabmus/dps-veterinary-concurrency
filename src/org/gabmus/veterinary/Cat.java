package org.gabmus.veterinary;

public class Cat extends Animal {

    public Cat(Veterinary vet) {
        super(vet);
    }

    @Override
    boolean canEnter() {
        return vet.getCats() == 0 && vet.getDogs() == 0;
    }
}
