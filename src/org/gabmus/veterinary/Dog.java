package org.gabmus.veterinary;

public class Dog extends Animal {

    public Dog(Veterinary vet) {
        super(vet);
    }

    @Override
    boolean canEnter() {
        return vet.getCats() == 0 && vet.getDogs() < 4;
    }
}
